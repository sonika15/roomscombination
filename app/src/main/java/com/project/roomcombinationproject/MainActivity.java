package com.project.roomcombinationproject;

import android.os.Bundle;
import android.util.Log;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
    public static final int RequestCode = 1;
    RecyclerView rvList;
    FloatingActionButton btnFloating;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvList = findViewById(R.id.rvList);
        btnFloating = findViewById(R.id.btnFloating);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setHasFixedSize(true);
        String myJson = inputStreamToString(this.getResources().openRawResource(R.raw.fixed_combination));
        getData(myJson);


    }

    public String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            return json;
        } catch (IOException e) {
            return null;
        }


    }

    public void getData(String result) {
        JSONObject response = null; //new JSONObject(a);
        try {
            response = new JSONObject(result);

            JSONObject hotelRoomResultJsonObject = response.getJSONObject("getHotelRoomResult");
            JSONArray jArray = hotelRoomResultJsonObject.getJSONArray("hotelRoomsDetails");
            String cancelPoli[] = new String[jArray.length()];
            JSONObject roomCombinationsJsonObject = hotelRoomResultJsonObject.getJSONObject("roomCombinations");
            String infoSource = roomCombinationsJsonObject.optString("infoSource");
            if (infoSource.equalsIgnoreCase("OpenCombination")) {
                JSONArray roomCombinationJsonArray = roomCombinationsJsonObject.optJSONArray("roomCombination");
                List<String> mystring = new ArrayList<>();
                int GroupId = 0;
                for (int i = 0; i < roomCombinationJsonArray.length(); i++) {
                    JSONObject roomIndex = roomCombinationJsonArray.getJSONObject(i);
                    JSONArray roomIndexArray = roomIndex.optJSONArray("roomIndex");
                    Log.wtf("OpenCombination-roomIndexArray", roomIndexArray.toString());
                    for (int j = 0; j < roomIndexArray.length(); j++) {
                        // Log.wtf("OpenCombination-index", roomIndexArray.getString(j));
                        int[] myindex = new int[roomIndexArray.length()];
                        myindex[j] = roomIndexArray.getInt(j);

                        Log.d("MyLOG", "onResponse: " + j + myindex[j]);

                        for (int ii = 0; ii < jArray.length(); ii++) {
                            JSONObject rooms = jArray.getJSONObject(ii);
                            JSONObject roomprice = rooms.getJSONObject("price");
                            String offeredPrice = roomprice.getString("publishedPriceRoundedOff");
                            String roomName = rooms.getString("roomTypeName");

                            int roomIndexOnHotelRoomDetails = rooms.getInt("roomIndex");
                            String rateplaneName = rooms.getString("ratePlanName");
                            if (roomIndexOnHotelRoomDetails == myindex[j]) {
                                cancelPoli[j] = rooms.getString("cancellationPolicy");
                                Log.d("MyLOG", "GroupId: " + GroupId + " roomIndex " + roomIndexOnHotelRoomDetails + " room name " + roomName + "  " + "OfferedPrice" + "  " + offeredPrice);
                            }
                        }
                    }

                }
            }

            if (infoSource.equalsIgnoreCase("FixedCombination")) {
                JSONArray roomCombinationJsonArray = roomCombinationsJsonObject.optJSONArray("roomCombination");
                List<String> fixedMyString = new ArrayList<>();
                int GroupId = 0;

                for (int i = 0; i < roomCombinationJsonArray.length(); i++) {
                    JSONObject jsonObject = roomCombinationJsonArray.getJSONObject(i);
//                    String index = jsonObject.getString("roomIndex").replace("[", "").replace("]", "");
//                    indexList.add(index);

                    JSONObject roomIndex = roomCombinationJsonArray.getJSONObject(i);
                    JSONArray roomIndexArray = roomIndex.optJSONArray("roomIndex");
                    Log.wtf("FixedCombination-roomIndexArray", roomIndexArray.toString());
                    for (int j = 0; j < roomIndexArray.length(); j++) {
//                        Log.wtf("roomIndexsonika", String.valueOf(roomIndexArray.get(j)));
//                        int roomIndex1 = Integer.parseInt(String.valueOf(roomIndexArray.get(j)));
//
//                        for (int k=0;k<jArray.length();k++){
//                            JSONObject jsonObject1 = jArray.getJSONObject(k);
//                            String indexRoom = jsonObject1.getString("roomIndex");
//                            Log.wtf("roomIndex",indexRoom);
//                            if (indexRoom.equals(roomIndex1)){
//                                String roomName = jsonObject1.getString("roomTypeName");
//                                Log.wtf("roomName",roomName);
//
//                            }
//                        }


                        //HotelRoomListingModel mainRoomListCount = new HotelRoomListingModel();
                        //Log.wtf("FixedCombination-index", roomIndexArray.getString(j));
                        int[] myindex = new int[roomIndexArray.length()];
                        myindex[j] = roomIndexArray.getInt(j);

                        //Log.d("MyLOG", "onResponse: " + j + myindex[j]);
                        //ArrayList<HotelRoomListingModel> hotelRoomPerRowArrayList = new ArrayList<>();
                        for (int ii = 0; ii < jArray.length(); ii++) {
                            JSONObject rooms = jArray.getJSONObject(ii);
                            JSONObject roomprice = rooms.getJSONObject("price");
                            String offeredPrice = roomprice.getString("publishedPriceRoundedOff");
                            String roomName = rooms.getString("roomTypeName");
                            int roomIndexOnHotelRoomDetails = rooms.getInt("roomIndex");
                            String rateplaneName = rooms.getString("ratePlanName");

                            cancelPoli[j] = rooms.getString("cancellationPolicy");
                            // Log.d("MyLOG", "GroupId: " + GroupId + " roomIndex " + roomIndexOnHotelRoomDetails + " room name " + roomName + "  " + "OfferedPrice" + "  " + offeredPrice);
//                            HotelRoomListingModel hotelRoomListingModel = new HotelRoomListingModel();
//                            hotelRoomListingModel.setRoomName(roomName);
//                            hotelRoomListingModel.setPublishedPriceRoundedOff(offeredPrice);
//                            hotelRoomPerRowArrayList.add(hotelRoomListingModel);
//                            mainRoomListCount.setHotelRoomListingModelArrayList(hotelRoomPerRowArrayList);
                            for (int l = 0; l < myindex.length; l++) {
                                RoomClass roomClass = new RoomClass();
                                if (roomIndexOnHotelRoomDetails == myindex[l]) {
                                    roomClass.setRoomName(roomName);
                                    Log.wtf("roomclass", roomClass.getRoomName());

                                }

                            }



                        }
                    }
                }


//            hotelRoomListingAdapter = new HotelRoomListingAdapter(HotelRoomListingActivity.this, finalHotelRoomListingArrayList);
//            Log.wtf("final", String.valueOf(finalHotelRoomListingArrayList.size()));
//            recyclerViewRoom.setHasFixedSize(true);
//            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//            recyclerViewRoom.setLayoutManager(layoutManager);
//            recyclerViewRoom.setAdapter(hotelRoomListingAdapter);


            }
        } catch (JSONException ex) {
            ex.printStackTrace();


        }
    }
}



